Rails.application.routes.draw do
  	mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
	#ciudades
	post "cities/destroy_city"
	resources :cities

	#departamentos
	post "departments/destroy_department"
	resources :departments

	#tipos de documentos
	post "document_types/destroy_document_type"
	resources :document_types

	#Agrupadores
	post "groupers/destroy_grouper"
	resources :groupers

	#Tenants
	post "tenants/destroy_tenant"
	resources :tenants

	#Users
	post "users/destroy_user"
	get "users/search"
	post "users/get_user_documents"
	post "users/set_user_documents"
	post "users/update_user_documents"
	post "users/destroy_user_documents"
	post "users/get_tenant_user"
	post "users/get_legal_representative"
	post "users/get_user_by_email"
	resources :users

	#Roles
	resources :roles

	#Documentos
	post "documents/destroy_document"
	post "documents/set_firm_document"
	post "documents/validate_token"
	post "documents/reject_document"
	post "documents/validate_reject_token"
	post "documents/send_notification"
	get "documents/search"
	resources :documents

	#Firmantes
	post "firmantes/validate_email"
	post "firmantes/validate_nit"
	post "firmantes/signatory_search"
	

	resources :firmantes
end
