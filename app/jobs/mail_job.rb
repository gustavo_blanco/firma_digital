class MailJob < ApplicationJob
  queue_as :default

	def perform(document,user)
		DocumentMailer.send_notifaction_expired_document(document,user).deliver!
	end
end
