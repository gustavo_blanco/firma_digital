class FirmanteService

	def self.validate_nit(nit)
		firmantes = Firmante.where("nit like '%#{nit}%'")
		if firmantes.blank?
			return true
		else
			return false
		end
	end

	def self.validate_email(email)
		firmantes = Firmante.where("email like '%#{email}%'")
		if firmantes.blank?
			return true
		else
			return false
		end
	end

	def self.get_firmante_name_by_token(document,token)
		event_name = ""
		firmante = Firmante.where("signer_token = '#{token}'").first
		if !firmante.blank?
			event_name = "Aceptación de documento #{document.name} por parte de #{firmante.nombre}"
		end
		return event_name
	end

	def self.set_signed_document(document_id,token)
		firmante = Firmante.where("document_id = #{document_id} and signer_token = '#{token}'").first
		if !firmante.blank?
			firmante.signed = true
			firmante.save
		end
	end

	def self.search_signer_by_nit(nit)
		firmante = Firmante.where("nit = ? and rol = 'Firmante'",nit).first
	end
end