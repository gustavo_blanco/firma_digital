class DocumentSignService
	require 'openssl'
	require 'stringio'
	require 'time'
	require 'prawn'
	begin
	    require 'origami'
	rescue LoadError
	    $: << File.join(__dir__, "../../lib")
	    require 'origami'
	end
	include Origami

	#require '/home/yeryie/wrappers_build/PDFNetWrappers/PDFNetC/Lib/PDFNetRuby'
	#require '/var/www/html/apps/firma_digital/wrappers_build/PDFNetWrappers/PDFNetC/Lib/PDFNetRuby'
	#require '../../../PDFNetC/Lib/PDFNetRuby'
	#include PDFNetRuby

	$stdout.sync = true

	#PDFNet.Initialize
	#PDFNet.SetColorManagement	 # Enable color management (required for PDFA validation).


	def self.sign(document,document_name,firmante,ip_address)
		#input_file = 'INV-1.pdf' #ruta del documento
		input_file = document
		document_name = document_name.split(".")[0]
		output_file = "/tmp/#{document_name}_firmado.pdf"
		puts "Generating a RSA key pair."
		key = OpenSSL::PKey::RSA.new 2048
		puts "Generating a self-signed certificate."
		name = OpenSSL::X509::Name.parse 'CN=origami/DC=example'
		#cert = OpenSSL::X509::Certificate.new
		cert = OpenSSL::X509::Certificate.new(File.read('cert/CertificadoCC.cer'))
		cert.public_key = key.public_key
		cert.sign key, OpenSSL::Digest::SHA256.new

		extension_factory = OpenSSL::X509::ExtensionFactory.new
		extension_factory.issuer_certificate = cert
		extension_factory.subject_certificate = cert
		cert.add_extension extension_factory.create_extension('basicConstraints', 'CA:TRUE', true)
		cert.add_extension extension_factory.create_extension('keyUsage', 'digitalSignature,keyCertSign')
		cert.add_extension extension_factory.create_extension('subjectKeyIdentifier', 'hash')

		#################################################
		# Read input PDF
		#################################################
		respaldo_file = "/tmp/#{document_name}_respaldo.pdf"
		result = DocumentService.edit_document(input_file,firmante)
		if !result.blank?
			result.each do |r|
				if !r[:x].blank?
					pdf3 = CombinePDF.load input_file
					now = Time.now
					# create a textbox and add it to the existing pdf on page 2
					#pdf3.pages[r[:page].to_i-1].textbox " #{firmante.nombre} | #{now.strftime("%d/%m/%Y %H:%M")} | #{ip_address}", height: 20, width: r[:width].to_f+150, y: r[:y].to_f-3, x:r[:x].to_f,
					#opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0],border_width: 1

					pdf3.pages[r[:page].to_i-1].textbox "#{firmante.nombre}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-5, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					pdf3.pages[r[:page].to_i-1].textbox "#{firmante.email}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-18, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					pdf3.pages[r[:page].to_i-1].textbox "#{now.strftime("%d/%m/%Y %H:%M")}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-35, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					pdf3.pages[r[:page].to_i-1].textbox "#{ip_address}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-50, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					# output the new pdf which now contains your dynamic data
					pdf3.save input_file
				end
			end
		end
		###fin invento
		pdf = PDF.read(input_file)
		page = pdf.get_page(1)
		#################################################
		# prepare annotation data ( visable time_stamp )
		#################################################
		##ensayo con otras medidas de firma
		if DocumentService.validate_all_signed_to_document(firmante)
			width = 300.0
			height = 50.0
			#fin ensayo
			x = page.MediaBox[2].to_f - width - height
			y = height
			size = 8
			now = Time.now
			text_annotation = Annotation::AppearanceStream.new
			text_annotation.Type = Origami::Name.new("XObject")
			text_annotation.Resources = Resources.new
			text_annotation.Resources.ProcSet = [Origami::Name.new("Text")]
			text_annotation.set_indirect(true)
			text_annotation.Matrix = [ 1, 0, 0, 1, 0, 0 ]
			text_annotation.BBox = [ 0, 0, width, height ]
			text_annotation.write("", x: size, y: (height/2)-(size/2), size: size)
			# Add signature annotation (so it becomes visibles in PDF document)
			signature_annotation = Annotation::Widget::Signature.new
			signature_annotation.Rect = Rectangle[llx: x, lly: y+height, urx: x+width, ury: y]
			#signature_annotation.Rect = Rectangle[llx: 100, lly: 100, urx: 300, ury: 200]
			signature_annotation.F = Annotation::Flags::PRINT
			signature_annotation.set_normal_appearance(text_annotation)
			page.add_annotation(signature_annotation)
			############################
			# Sign the PDF with the specified keys
			pdf.sign( cert,
			          key,
			          method: 'adbe.pkcs7.sha1',
			          annotation: signature_annotation
			          )
			# Save the resulting file
			pdf.save(output_file)
			puts "PDF file saved as #{output_file}."
			# Now that we have signed and saved, lets re-open it and prove the concept
			document = PDF.read(output_file)
			document.signature.inspect
			begin
			  puts "******"
			  puts document.verify(trusted_certs: [cert])
			  puts "******"
			rescue StandardError => e
			  puts e.message
			end
		
			if !document.signature
				#cert_2 = OpenSSL::X509::Certificate.new(File.read('cert_123.crt'))
				cert_2 = OpenSSL::X509::Certificate.new(File.read('cert/CertificadoCC.cer'))
				puts "$" * 40
				  puts document.verify(trusted_certs: [cert_2])
				puts "$" * 40
			end
		else
			pdf.save(output_file)
		end##end firmado
		return "OK"
	end


	def self.sign_antiguo(document,document_name,firmante,ip_address)
		input_file = document
		document_name = document_name.split(".")[0]
		output_file = "/tmp/#{document_name}_firmado.pdf"
		#################################################
		# Read input PDF
		#################################################
		respaldo_file = "/tmp/#{document_name}_respaldo.pdf"
		result = DocumentService.edit_document(input_file,firmante)
		if !result.blank?
			result.each do |r|
				if !r[:x].blank?
					pdf3 = CombinePDF.load input_file
					now = Time.now
					# create a textbox and add it to the existing pdf on page 2
					#pdf3.pages[r[:page].to_i-1].textbox " #{firmante.nombre} | #{now.strftime("%d/%m/%Y %H:%M")} | #{ip_address}", height: 20, width: r[:width].to_f+150, y: r[:y].to_f-3, x:r[:x].to_f,
					#opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0],border_width: 1

					pdf3.pages[r[:page].to_i-1].textbox "#{firmante.nombre}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-5, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					pdf3.pages[r[:page].to_i-1].textbox "#{firmante.email}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-18, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					pdf3.pages[r[:page].to_i-1].textbox "#{now.strftime("%d/%m/%Y %H:%M")}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-35, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					pdf3.pages[r[:page].to_i-1].textbox "#{ip_address}", height: 15, width: r[:width].to_f+60, y: r[:y].to_f-50, x:r[:x].to_f,
					opacity: 1,box_color: [255,255,255],text_padding: 1,font_size: 8,text_align: "right",border_color: [0,0,0]
					# output the new pdf which now contains your dynamic data
					pdf3.save input_file
				end
			end
		end
		pdf = PDF.read(input_file)
		pdf.save(output_file)
		###fin invento
	end

	def self.set_certificate(document,document_name,firmante)
		puts "Generating a RSA key pair."
		key = OpenSSL::PKey::RSA.new 2048
		puts "Generating a self-signed certificate."
		name = OpenSSL::X509::Name.parse 'CN=origami/DC=example'
		#cert = OpenSSL::X509::Certificate.new
		cert = OpenSSL::X509::Certificate.new(File.read('cert/CertificadoCC.cer'))
		cert.public_key = key.public_key
		cert.sign key, OpenSSL::Digest::SHA256.new
		extension_factory = OpenSSL::X509::ExtensionFactory.new
		extension_factory.issuer_certificate = cert
		extension_factory.subject_certificate = cert
		cert.add_extension extension_factory.create_extension('basicConstraints', 'CA:TRUE', true)
		cert.add_extension extension_factory.create_extension('keyUsage', 'digitalSignature,keyCertSign')
		cert.add_extension extension_factory.create_extension('subjectKeyIdentifier', 'hash')
		#document = document.split("_firmado.pdf")[0]
		input_file = "#{document}_firmado_pdfa.pdf"
		#input_file = document
		document_name = document_name.split(".")[0]
		output_file = "/tmp/#{document_name}_cerficado.pdf"
####prueba pdfNetruby
		pdf = PDFDoc.new(input_file);
		page = pdf.GetPage(1);
		# Create a text field that we can lock using the field permissions feature.
		#annot1 = TextWidget.Create(pdf.GetSDFDoc, Rect.new(50, 550, 350, 600), "");
		#page1.AnnotPushBack(annot1);
		certification_sig_field = pdf.CreateDigitalSignatureField('cert/CertificadoCC.cer');
		widgetAnnot = SignatureWidget.Create(pdf, Rect.new(0, 100, 200, 150), certification_sig_field);
		page.AnnotPushBack(widgetAnnot);
		certification_sig_field.SetDocumentPermissions(DigitalSignatureField::E_annotating_formfilling_signing_allowed);
		certification_sig_field.SetFieldPermissions(DigitalSignatureField::E_include, ['cert/CertificadoCC.cer']);
		certification_sig_field.CertifyOnNextSave(cert.sign,key);
		pdf.Save(output_file, 0);
##preuba pdfnetruby
		#pdf = PDF.read(input_file)
		#page = pdf.get_page(1)
=begin
		if DocumentService.validate_all_signed_to_document(firmante)
			width = 300.0
			height = 50.0
			#fin ensayo
			#x = page.MediaBox[2].to_f - width - height
			x = 1000 - width - height
			y = height
			size = 8
			now = Time.now
			text_annotation = Annotation::AppearanceStream.new
			text_annotation.Type = Origami::Name.new("XObject")
			text_annotation.Resources = Resources.new
			text_annotation.Resources.ProcSet = [Origami::Name.new("Text")]
			text_annotation.set_indirect(true)
			text_annotation.Matrix = [ 1, 0, 0, 1, 0, 0 ]
			text_annotation.BBox = [ 0, 0, width, height ]
			text_annotation.write("", x: size, y: (height/2)-(size/2), size: size)
			# Add signature annotation (so it becomes visibles in PDF document)
			signature_annotation = Annotation::Widget::Signature.new
			signature_annotation.Rect = Rectangle[llx: x, lly: y+height, urx: x+width, ury: y]
			#signature_annotation.Rect = Rectangle[llx: 100, lly: 100, urx: 300, ury: 200]
			signature_annotation.F = Annotation::Flags::PRINT
			signature_annotation.set_normal_appearance(text_annotation)
			#page.add_annotation(signature_annotation)
			############################
			# Sign the PDF with the specified keys
			pdf.sign( cert,
			          key,
			          method: 'adbe.pkcs7.sha1',
			          annotation: signature_annotation
			          )
			# Save the resulting file
			pdf.save(output_file)
			puts "PDF file saved as #{output_file}."
			# Now that we have signed and saved, lets re-open it and prove the concept
			document = PDF.read(output_file)
			document.signature.inspect
			begin
			  puts "******"
			  puts document.verify(trusted_certs: [cert])
			  puts "******"
			rescue StandardError => e
			  puts e.message
			end
		
			if !document.signature
				#cert_2 = OpenSSL::X509::Certificate.new(File.read('cert_123.crt'))
				cert_2 = OpenSSL::X509::Certificate.new(File.read('cert/CertificadoCC.cer'))
				puts "$" * 40
				  puts document.verify(trusted_certs: [cert_2])
				puts "$" * 40
			end
		else
			pdf.save(output_file)
		end##end firmado
=end
		return "OK"
	end

end