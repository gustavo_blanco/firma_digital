class DocumentService

	def self.send_notifaction_expired_documents
		today = Date.today
		today = today + 8 
		status = Status.find_by_name("PENDIENTE POR FIRMAS")
		status_r = Status.find_by_name("ACTIVO")
		documents = Document.where("fecha_limite_aprobacion <= '#{today}' and
			(status_id is null or status_id <> #{status.id} or status_id <> #{status_r.id})")
		if !documents.blank?
			documents.each do |document|
				users = User.where("tenant_id = #{document.tenant_id} and role_name = 'ADMINISTRADOR'")
				if !users.blank?
					users.each do |user|
						MailJob.perform_later(document,user)
						#DocumentMailer.send_notifaction_expired_document(document,user).deliver!
					end
				end
			end
		end
	end


	def self.set_user_documents(document,ip_address)
		response = "ERROR"
		firmante1_bandera = false
		new_document = Document.new
		new_document.name = document["name"]
		new_document.grouper_id = document["grouper_id"]
		new_document.objeto_costo = document["objeto_costo"]
		new_document.radicado = document["radicado"]
		new_document.plataforma_origen = document["plataforma_origen"]
		new_document.fecha_limite_aprobacion = document["fecha_limite_aprobacion"]
		tenant = Tenant.find(document["tenant_id"])
		if !tenant.blank?
			new_document.usuario_company = tenant.name
		end
		new_document.tenant_id = document["tenant_id"]
		new_document.flujo = document["flujo"]
		status = Status.find_by_name("PENDIENTE POR FIRMAS")
		new_document.status_id = status.id
		if !document["attachment"].blank?
			new_document.attachment = document["attachment"]
		end
		if new_document.save
			EventLogService.set_event_log("Creación de documento #{new_document.name}",new_document.id,ip_address)
			i = 0
			while !document["firmantes"][i.to_s].blank?
				firmante = Firmante.new
				firmante.document_id = new_document.id
				firmante.nombre = document["firmantes"][i.to_s]["nombre"]
				firmante.nit = document["firmantes"][i.to_s]["nit"]
				firmante.email = document["firmantes"][i.to_s]["email"]
				firmante.celular = document["firmantes"][i.to_s]["celular"]
				firmante.rol = document["firmantes"][i.to_s]["rol"]
				firmante.posicion_firma = document["firmantes"][i.to_s]["posicion_firma"]
				firmante.orden_firmante = document["firmantes"][i.to_s]["orden_firmante"]
				if firmante.rol == "Representante Legal"
					firmante.user_id = document["firmantes"][i.to_s]["user_id"]
					user = User.find(firmante.user_id)
					if !user.blank?
						firmante.token_login = user.signer_token
						firmante.signer_token = user.signer_token
					end
				end
				if firmante.rol != "Representante Legal"
					firmante.signer_token = SecureRandom.uuid
					firmante.token_login = SecureRandom.uuid
				end
				if firmante.rol != "Firmante" && firmante.rol != "Representante Legal"
					firmante.signed = true
				end
				
				#firmante.reject_token = SecureRandom.uuid
				if firmante.save
					event_name = "Envío de token para firmar documento a #{firmante.nombre}"
					if new_document.flujo == "Secuencial"
						if i == 0
							if firmante.rol == "Firmante"
								DocumentMailer.send_token(new_document, firmante).deliver!
								EventLogService.set_event_log(event_name,new_document.id,ip_address)
								firmante1_bandera = true
							elsif firmante.rol == "Representante Legal"
								DocumentMailer.send_email_to_signed(new_document, firmante).deliver!
								EventLogService.set_event_log(event_name,new_document.id,ip_address)
								firmante1_bandera = true
							elsif firmante.rol != "Firmante"
								DocumentMailer.send_notifaction(new_document, firmante).deliver!
							end
						else
							if firmante1_bandera == false && firmante.rol == "Firmante"
								firmante1_bandera = true
							 	DocumentMailer.send_token(new_document, firmante).deliver!
								EventLogService.set_event_log(event_name,new_document.id,ip_address)
							elsif firmante1_bandera == false && firmante.rol == "Representante Legal"
								firmante1_bandera = true
							 	DocumentMailer.send_email_to_signed(new_document, firmante).deliver!
								EventLogService.set_event_log(event_name,new_document.id,ip_address)
							else
								if firmante.rol != "Firmante" && firmante.rol != "Representante Legal"
									DocumentMailer.send_notifaction(new_document, firmante).deliver!
								end
							end 
						end
					else
						if firmante.rol == "Firmante"	
							DocumentMailer.send_token(new_document, firmante).deliver!
							EventLogService.set_event_log(event_name,new_document.id,ip_address)
						elsif firmante.rol == "Representante Legal"
							DocumentMailer.send_email_to_signed(new_document, firmante).deliver!
							EventLogService.set_event_log(event_name,new_document.id,ip_address)
						else
							DocumentMailer.send_notifaction(new_document, firmante).deliver!
						end
					end
					# Si el ROL del firmante es Representate Legal lo guardamos con ROL firmante
					if firmante.rol == "Representante Legal"
						firmante.rol = "Firmante"
					end
					firmante.save
				end 
				i +=1
			end
		end
		return new_document
	end

	def self.get_doc_status_name(document)
		status_name = ""
		if document.flujo == "Secuencial"
			if !document.status_id.blank?
		    	status_name = Status.find(document.status_id).name
	  		end
	  		if status_name == "PENDIENTE POR FIRMAS"
	  			firmantes = Firmante.where("document_id = #{document.id} and (rol = 'Firmante' or 
	  				rol = 'Representante Legal')").order("orden_firmante")
	  			if !firmantes.blank?
	  				cant_signed = 1
	  				firmantes.each do |firmante|
	  					if firmante.signed == true
	  						cant_signed += 1
	  					end
	  				end
	  				if cant_signed <= firmantes.size
	  					status_name = status_name.to_s + " " + cant_signed.to_s
	  				end
	  			end
	  		end
		else
			if !document.status_id.blank?
		    	status_name = Status.find(document.status_id).name
	  		end
		end
		return status_name
	end


	def update_user_documents(document)
		response = "ERROR"
		new_document = Document.find(document["id"])
		if new_document.blank?
			new_document = Document.new
		end
		new_document.name = document["name"]
		new_document.grouper_id = document["grouper_id"]
		if !document["attachment"].blank?
			new_document.attachment = document["attachment"]
		end
		if new_document.save
			response = "OK"
		end
		return new_document
	end

	def destroy_user_documents(id)
		@response = "ERROR"
		status = Status.find_by_name("ANULADO")
	    if !status.blank?
	      document = Document.find(id)
	      document.status_id = status.id
	      if document.save
	        @response = "OK"
	      end
	    end 
	    return @response
	end

	def self.accept_document(document,document_name,document_id,firmante,ip_address)
		download_pdf(document,document_name)
		response = DocumentSignService.sign("/tmp/#{document_name}",document_name,firmante,ip_address)
		document = upload_document(document_name,document_id,firmante)
		return document
	end

	def self.accept_document_antiguo(document,document_name,document_id,firmante,ip_address)
		download_pdf(document,document_name)
		#se colocan estampas
		DocumentSignService.sign("/tmp/#{document_name}",document_name,firmante,ip_address)
		document = validate_signed(document_id,firmante)
		status = Status.find(document.status_id)
		if status.name == "FINALIZADO"
			document_converted = convert_document(document_name,document_id,firmante)
			document_name = document_converted.attachment_file_name.split("_firmado_pdfa.pdf")[0]
			#document_name = document_name+"_cerficado.pdf"
			document = Document.find(document_id)
			download_pdf(document.attachment,document.attachment_file_name)
			DocumentSignService.set_certificate("/tmp/#{document_name}",document_name,firmante)
			document = upload_document(document_name,document_id,"OK")
		else
			document = upload_document(document_name,document_id,"estampas")
		end
		#######
		return document
	end

	def self.validate_token(document_id,token,email)
		joins_str =" LEFT JOIN firmantes f on f.document_id = documents.id"
		where_str = "documents.id = #{document_id} and f.signer_token = '#{token}' and f.email = '#{email}'"
		select_str = "documents.id,f.id,signed"
		document = Document.select(select_str).joins(joins_str).where(where_str).first
		if !document.blank?
			if document.signed == true
				return false
			else
				return true
			end
		else
			return false
		end
	end

	def self.upload_document(document_name,document_id,firmante)
		d = Document.find(document_id)
		document_name = document_name.split(".")[0]
		document = validate_signed(document_id,firmante)
		status = Status.find(document.status_id)
		filename = '/tmp/'+document_name+"_firmado.pdf"
		if status.name == "FINALIZADO"
			#ConvertPdfToPdfaService.convert(document_name)
			#filename = '/tmp/'+document_name+"_firmado_pdfa.pdf"
		end
		file = File.open(filename)
		d.attachment = file
		d.save!
		file.close
		return d		
	end

	def self.upload_document_antiguo(document_name,document_id,response)
		d = Document.find(document_id)
		document_name = document_name.split(".")[0]
		if response == "OK"
			filename = '/tmp/'+document_name+"_cerficado.pdf"
		else
			filename = '/tmp/'+document_name+"_firmado.pdf"
		end
		file = File.open(filename)
		d.attachment = file
		d.save!
		file.close
		return d
	end

	def self.convert_document(document_name,document_id,firmante)
		response = "ERROR"
		d = Document.find(document_id)
		#document_name = document_name.split("_firmado.pdf")[0]
		filename = '/tmp/'+document_name+"_firmado.pdf"
		ConvertPdfToPdfaService.convert(document_name)
		document_name = document_name.split(".")[0]
		#raise document_name.inspect
		filename = '/tmp/'+document_name+"_firmado_pdfa.pdf"
		file = File.open(filename)
		d.attachment = file
		d.save!
		file.close
		return d
	end

	def self.download_pdf_resp(object_key,document_name)
		access_key = "AKIAIRI2LAO757MQ3LIA"
		secret_key = "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"
		creds = Aws::Credentials.new(access_key, secret_key)
  		s3 = Aws::S3::Client.new(region: 'us-east-1', credentials: creds)
		#s3 = Aws::S3::Client.new
		File.open("/tmp/#{document_name}",'wb') do |file|
			resp = s3.get_object({bucket: 'conconcreto-firmadigital', key: object_key}, target: file)
		end		
	end

	def self.download_pdf(object,document_name)
		fog = Fog::Storage.new({provider: 'Google',google_storage_access_key_id: 'GOOGEQUAQ6KMVPFG6QL4ZWY6', 
			google_storage_secret_access_key: '8s17TS6llERWzIvFBfj3al3TjIBhIM4ApV4bu4n3'})
		#raise fog.directories.get("mabella-fdigital").files.get(object.path).body.inspect
		#aise fog.get_object_acl('mabella-fdigital',object.public_url).inspect
		File.open("/tmp/#{document_name}",'wb') do |file|
			file.write(fog.directories.get("mabella-fdigital").files.get(object.path).body)
		end
	end

	def self.validate_signed(document_id,firmante)
		document = Document.find(document_id)
		if document.flujo == "Secuencial"
			firmantes = Firmante.where("document_id = #{document_id}").order("orden_firmante")
		else
			firmantes = Firmante.where("document_id = #{document_id}").order("id")	
		end
		if !firmante.blank?
			signed_signers = 1
		else
			signed_signers = 0
		end
		if !firmantes.blank?
			firmantes.each do |firmante|
				if firmante.signed == true
					signed_signers += 1
				end
			end
			if firmantes.size == signed_signers
				status = Status.find_by_name("FINALIZADO")
				document.status_id = status.id
			end
		end
		return document
	end

	def self.validate_all_signed(document_id)
		document = Document.find(document_id)
		if document.flujo == "Secuencial"
			firmantes = Firmante.where("document_id = #{document_id}").order("orden_firmante")
		else
			firmantes = Firmante.where("document_id = #{document_id}").order("id")	
		end
		signed_signers = 0
		if !firmantes.blank?
			firmantes.each do |firmante|
				if firmante.signed == true
					signed_signers += 1
				end
			end
			if firmantes.size == signed_signers
				status = Status.find_by_name("FINALIZADO")
				document.status_id = status.id
				document.save
				firmantes.each do |firmante|
					if firmante.rol == "Representante Legal"
						DocumentMailer.send_confirmation_rp(document, firmante).deliver!
					else	
						DocumentMailer.send_confirmation(document, firmante).deliver!
					end
				end
			end
		end
		return document
	end

	def self.validate_all_signed_to_document(firmante)
		document = Document.find(firmante.document_id)
		if document.flujo == "Secuencial"
			firmantes = Firmante.where("document_id = #{firmante.document_id}").order("orden_firmante")
		else
			firmantes = Firmante.where("document_id = #{firmante.document_id}").order("id")	
		end
		signed_signers = 0
		if !firmantes.blank?
			firmantes.each do |firmante|
				if firmante.signed == true
					signed_signers += 1
				end
			end
			if (firmantes.size.to_i - signed_signers.to_i) == 1
				return true
			else
				return false
			end
		end
	end

	def self.edit_document(input_file,firmante)
		result = Array.new
		if File.exists?(input_file)
			file = File.open(input_file)
			pages = PDFTextProcessor.process(file)
		  	if !pages.blank?
			  	pages.each do |page|
			  		if !page[:lines].blank?
			  			page[:lines].each do |line|
			  				if !line[:text_groups].blank?
			  					line[:text_groups].each do |text_group|
			  						if firmante.posicion_firma.to_s == text_group[:text].gsub(/\s+/, '')
			  						#if firmante.posicion_firma.to_s == text_group[:text]
			  						#if text_group[:text].include?("1_etnamrif_noicisop$")
			  							result << {y:line[:y],x:text_group[:x],width:text_group[:width],
			  								text:firmante.nombre,page: page[:page]}
			  						elsif firmante.posicion_firma.to_s == text_group[:text].reverse!.gsub(/\s+/, '')
			  							result << {y:line[:y],x:text_group[:x] + text_group[:width] - 8,width:text_group[:width],
			  								text:firmante.nombre,page: page[:page]}
			  						end
			  					end
			  				end
			  			end
			  		end
			  	end
		  	end
			#raise pages.first[:lines].inspect
		end
		return result
	end
		

end##

require 'pdf-reader'

class CustomPageLayout < PDF::Reader::PageLayout
  attr_reader :runs

  # we need to filter duplicate characters which seem to be caused by shadowing
  def group_chars_into_runs(chars)
    # filter out duplicate chars before going on with regular logic,
    # seems to happen with shadowed text
    chars.uniq! {|val| {x: val.x, y: val.y, text: val.text}}
    super
  end
end

class PageTextReceiverKeepSpaces < PDF::Reader::PageTextReceiver
  # We must expose the characters and mediabox attributes to instantiate PageLayout
  attr_reader :characters, :mediabox

  private
  def internal_show_text(string)
    if @state.current_font.nil?
      raise PDF::Reader::MalformedPDFError, "current font is invalid"
    end
    glyphs = @state.current_font.unpack(string)
    glyphs.each_with_index do |glyph_code, index|
      # paint the current glyph
      newx, newy = @state.trm_transform(0,0)
      utf8_chars = @state.current_font.to_utf8(glyph_code)

      # apply to glyph displacment for the current glyph so the next
      # glyph will appear in the correct position
      glyph_width = @state.current_font.glyph_width(glyph_code) / 1000.0
      th = 1
      scaled_glyph_width = glyph_width * @state.font_size * th

      # modification to the original pdf-reader code which otherwise accidentally removes spaces in some cases
      # unless utf8_chars == SPACE
      @characters << PDF::Reader::TextRun.new(newx, newy, scaled_glyph_width, @state.font_size, utf8_chars)
      # end

      @state.process_glyph_displacement(glyph_width, 0, utf8_chars == SPACE)
    end
  end
end

class PDFTextProcessor
  MAX_KERNING_DISTANCE = 10 # experimental value

  # pages may specify which pages to actually parse (zero based)
  #   [0, 3] will process only the first and fourth page if present
  def self.process(pdf_io, pages = nil)
    pdf_io.rewind
    reader = PDF::Reader.new(pdf_io)
    fail 'Could not find any pages in the given document' if reader.pages.empty?
    processed_pages = []
    text_receiver = PageTextReceiverKeepSpaces.new
    requested_pages = pages ? reader.pages.values_at(*pages) : reader.pages
    requested_pages.each do |page|
      unless page.nil?
        page.walk(text_receiver)
        runs = CustomPageLayout.new(text_receiver.characters, text_receiver.mediabox).runs

        # sort text runs from top left to bottom right
        # read as: if both runs are on the same line first take the leftmost, else the uppermost - (0,0) is bottom left
        runs.sort! {|r1, r2| r2.y == r1.y ? r1.x <=> r2.x : r2.y <=> r1.y}

        # group runs by lines and merge those that are close to each other
        lines_hash = {}
        runs.each do |run|
          lines_hash[run.y] ||= []
          # runs that are very close to each other are considered to belong to the same text "block"
          if lines_hash[run.y].empty? || (lines_hash[run.y].last.last.endx + MAX_KERNING_DISTANCE < run.x)
            lines_hash[run.y] << [run]
          else
            lines_hash[run.y].last << run
          end
        end
        lines = []
        lines_hash.each do |y, run_groups|
          lines << {y: y, text_groups: []}
          run_groups.each do |run_group|
            group_text = run_group.map { |run| run.text }.join('').strip
            lines.last[:text_groups] << ({
              x: run_group.first.x,
              width: run_group.last.endx - run_group.first.x,
              text: group_text,
            }) unless group_text.empty?
          end
        end
        # consistent indexing with pages param and reader.pages selection
        processed_pages << {page: page.number, lines: lines}
      end
    end
    processed_pages
  end
end

