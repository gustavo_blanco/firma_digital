class UserService

	def initialize()
    end

	def get_user_documents(user_id)
		select_str = "documents.id,documents.name,documents.document_type_id,documents.grouper_id,
			documents.attachment_file_name,du.user_id"
		join_str = "JOIN document_users du on du.document_id = documents.id"
		query_str = "du.user_id = #{user_id} and documents.status_id is null"
		documents = Document.select(select_str).joins(join_str).where(query_str)
		return documents
	end

	def set_user_documents(document)
		response = "ERROR"
		new_document = Document.new
		new_document.name = document["name"]
		new_document.grouper_id = document["grouper_id"]
		if !document["attachment"].blank?
			new_document.attachment = document["attachment"]
		end
		if new_document.save
			user_document = DocumentUser.new
			user_document.user_id = document["user_id"]
			user_document.document_id = new_document.id
			if user_document.save
				response = "OK"
			end
		end
		return new_document
	end


	def update_user_documents(document)
		response = "ERROR"
		new_document = Document.find(document["id"])
		if new_document.blank?
			new_document = Document.new
		end
		new_document.name = document["name"]
		new_document.grouper_id = document["grouper_id"]
		if !document["attachment"].blank?
			new_document.attachment = document["attachment"]
		end
		if new_document.save
			response = "OK"
		end
		return new_document
	end

	def destroy_user_documents(id)
		@response = "ERROR"
		status = Status.find_by_name("ANULADO")
	    if !status.blank?
	      document = Document.find(id)
	      document.status_id = status.id
	      if document.save
	        @response = "OK"
	      end
	    end 
	    return @response
	end
end