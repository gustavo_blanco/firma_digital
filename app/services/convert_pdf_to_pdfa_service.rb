class ConvertPdfToPdfaService
	#---------------------------------------------------------------------------------------
	# Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
	# Consult LICENSE.txt regarding license information.
	#---------------------------------------------------------------------------------------

	require '/home/yeryie/wrappers_build/PDFNetWrappers/PDFNetC/Lib/PDFNetRuby'
	#require '/var/www/html/apps/firma_digital/wrappers_build/PDFNetWrappers/PDFNetC/Lib/PDFNetRuby'
	#require '../../../PDFNetC/Lib/PDFNetRuby'
	include PDFNetRuby

	$stdout.sync = true

	PDFNet.Initialize
	PDFNet.SetColorManagement	 # Enable color management (required for PDFA validation).

	#---------------------------------------------------------------------------------------
	# The following sample illustrates how to parse and check if a PDF document meets the
	# PDFA standard, using the PDFACompliance class object. 
	#---------------------------------------------------------------------------------------

	# Relative path to the folder containing the test files.
	input_path = "../../TestFiles/"
	output_path = "../../TestFiles/Output/"

	def self.convert(document_name)
		PDFNet.Initialize
		PDFNet.SetColorManagement
		document_name = document_name.split(".")[0]
		filename = '/tmp/'+document_name+"_firmado.pdf"
		outfilename = '/tmp/'+document_name+"_firmado_pdfa.pdf"
		pdf_a = PDFACompliance.new(true, filename, nil, PDFACompliance::E_Level2B, 0, 0, 10)
		#pdf_a = PDFACompliance.new(true, filename, nil, PDFACompliance::E_Level2B, 0, 10)
		pdf_a.SaveAs(outfilename, true)
		#PrintResults(pdf_a, document_name+"_firmado_pdfa.pdf")
		puts "PDFACompliance test completed."
		#pdf_a.Destroy
	end

	def self.PrintResults(pdf_a, filename)
		err_cnt = pdf_a.GetErrorCount
		if err_cnt == 0
			puts filename + ": OK."
		else
			puts filename + " is NOT a valid PDFA."	
			i = 0
			while i < err_cnt do
				c = pdf_a.GetError(i)
				str1 = " - e_PDFA " + c.to_s + ": " + PDFACompliance.GetPDFAErrorMessage(c) + "."
				if true
					num_refs = pdf_a.GetRefObjCount(c)
					if num_refs > 0
						str1 = str1 + "\n   Objects: "
						j = 0
						while j < num_refs do
							str1 = str1 + pdf_a.GetRefObj(c, j).to_s
							if j < num_refs-1
								str1 = str1 + ", "
							end
							j = j + 1
						end
					end
				end
				puts str1
				i = i + 1
			end
			puts "\n"
		end
	end
	
	
end