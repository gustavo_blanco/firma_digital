class EventLogService

	require 'socket'
	
	def self.set_event_log(event_name,document_id,ip_address)
		response = "ERROR"
		event_log = EventLog.new
		event_log.name = event_name
		event_log.document_id = document_id
		#event_log.ip_address = Socket.ip_address_list.detect(&:ipv4_private?)&.ip_address
		event_log.ip_address = ip_address
		if event_log.save
			response = "OK"
		end
		return response
	end

	def self.set_event_next_firmante(document,token,ip_address)
		firmante = Firmante.where("signer_token = '#{token}' and document_id = #{document.id}").first
		if !firmante.blank?
			next_firmante = Firmante.where("document_id = #{document.id} and 
				(rol= 'Firmante' or rol = 'Representante Legal') and orden_firmante > #{firmante.orden_firmante}").order("orden_firmante").first
			if !next_firmante.blank?
				event_name = "Envío de token para firmar documento a #{next_firmante.nombre}"
				if next_firmante.rol == 'Representante Legal'
					DocumentMailer.send_email_to_signed(document, next_firmante).deliver!
				else
					DocumentMailer.send_token(document, next_firmante).deliver!
				end
				EventLogService.set_event_log(event_name,document.id,ip_address)
			end
		end
	end
end