class DocumentMailer < ActionMailer::Base
   def load_settings
      if Rails.env == 'production'
         DocumentMailer.smtp_settings = {
          :address              => 'smtp.office365.com',
          :port                 => 587,
          :domain               => "dtech.com.co",
          :user_name            => "Portales@conconcreto.com",
          :password             => "twu5cksxvjMpwGue1213@",
          :authentication       => 'login'
         }
      end
   end

   def send_token(document, firmante)
      @document = document
      @firmante = firmante
      @tenant = Tenant.find(document.tenant_id)
      load_settings
      mail(to: firmante.email, from: "Portales@conconcreto.com", subject: "Token para firmar documento - #{@document.name}")
   end

   def send_email_to_signed(document, firmante)
      @document = document
      @firmante = firmante
      @tenant = Tenant.find(document.tenant_id)
      load_settings
      mail(to: firmante.email, from: "Portales@conconcreto.com", subject: "Token para firmar documento - #{@document.name}")
   end

   def send_notifaction(document, firmante)
      @document = document
      @firmante = firmante
      load_settings
      mail(to: firmante.email, from: "Portales@conconcreto.com", subject: "Nuevo documento creado - #{@document.name}")
   end

   def send_confirmation(document, firmante)
      @document = document
      @firmante = firmante
      load_settings
      mail(to: @firmante.email, from: "Portales@conconcreto.com", subject: "Documento Aceptado por todos los firmantes - #{@document.name}")
   end

   def send_confirmation_rp(document, firmante)
      @document = document
      @firmante = firmante
      load_settings
      mail(to: @firmante.email, from: "Portales@conconcreto.com", subject: "Documento Aceptado por todos los firmantes - #{@document.name}")
   end

   def send_reject_document_confirmation(document,firmante_rejected,firmante)
      @document = document
      @firmante = firmante_rejected
      @current_firmante = firmante
      load_settings
      mail(to: firmante.email, from: "Portales@conconcreto.com", subject: "Documento Rechazado - #{@document.name}")
   end

   def send_reject_document_confirmation_admin(document,user,firmante)
      @document = document
      @current_user = user
      @firmante = firmante
      load_settings
      mail(to: firmante.email, from: "Portales@conconcreto.com", subject: "Documento Rechazado - #{@document.name}")
   end

   def send_notifaction_expired_document(document,user)
      @document = document
      @current_user = user
      load_settings
      mail(to: @current_user.email, from: "Portales@conconcreto.com", subject: "Documento "+document.name+" esta proximo a llegar su fecha limite de aprobación")
   end

end
