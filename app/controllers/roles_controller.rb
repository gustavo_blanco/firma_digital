class RolesController < ApplicationController
  before_action :set_role, only: [:show, :update, :destroy]

  # GET /tenants
  def index
  	current_user = User.where("email = '#{params[:email]}'").first
  	if !current_user.blank? && current_user.all_tenant == true
    	roles = Role.order("id")
    elsif !current_user.blank?
    	role = Role.find_by_name("ADMINISTRADOR")
    	if current_user.role_id == role.id
    		roles = Role.order("id")
    	else
    		roles = Role.where("name = 'REPRESENTANTE_LEGAL'")
    	end
    end
    json_response(roles)
  end

  def show
  	json_response(@role)
  end

  private

  def role_params
    # whitelist params
    params.require(:role).permit(:name)
  end

  def set_role
    @role = Role.find(params[:id])
  end
end
