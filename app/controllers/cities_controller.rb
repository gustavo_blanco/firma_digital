class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :update]
  before_action :authenticate_user!
  # GET /cities
  def index
    status = Status.find_by_name("ANULADO")
    if !params[:page].blank?
      @cities = City.where("(status_id is null or status_id <> #{status.id}) and 
        tenant_id = #{params[:tenant_id]}").
      paginate(:page =>params[:page],:per_page => 10).order(:name)
      @total = @cities.total_entries 
      @cities = ActiveModelSerializers::SerializableResource.new(@cities, each_serializer: CitySerializer)
      render json: {cities:@cities, total:@total}
    else
      @cities = City.where("(status_id is null or status_id <> #{status.id}) and 
        tenant_id = #{params[:tenant_id]}").order('name asc')
      json_response(@cities)
    end
  end

  # POST /cities
  def create
    @city = City.create!(city_params)
    json_response(@city, :created)
  end

  # GET /cities/:id
  def show
    json_response(@city)
  end

  # PUT /cities/:id
  def update
    @city.update(city_params)
    head :no_content
  end

  # DELETE /cities/:id

  def destroy_city
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      city = City.find(params[:id])
      city.status_id = status.id
      if city.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  def destroy
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      city = City.find(params[:id])
      city.status_id = status.id
      if city.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  private

  def city_params
    # whitelist params
    params.require(:city).permit(:name,:department_id,:tenant_id)
  end

  def set_city
    current_user = User.where("email = '#{params[:email]}'").first
    if current_user.role_name == "ADMINISTRADOR"
      @city = City.where("id = #{params[:id]} and tenant_id = #{current_user.tenant_id}").first
    end
  end
end
