class TenantsController < ApplicationController
  before_action :set_tenant, only: [:show, :update, :destroy]
  before_action :authenticate_user!

  # GET /tenants
  def index
    current_user = User.where("email = '#{params[:user_email]}'").first
    status = Status.find_by_name("ANULADO")
    if !current_user.blank? && current_user.all_tenant == true
      if !params[:page].blank?
        @tenants = Tenant.where("status_id is null or status_id <> #{status.id}").
        paginate(:page =>params[:page],:per_page => 10).order(:name)
        @total = @tenants.total_entries
      else
        @tenants = Tenant.where("status_id is null or status_id <> #{status.id}").order("name")
        @total = @tenants.size
      end
    else
      if !params[:page].blank?
        @tenants = Tenant.where("(status_id is null or status_id <> #{status.id}) and 
        id = #{params[:tenant_id]}").paginate(:page =>params[:page],:per_page => 10).order(:name)
        @total = @tenants.total_entries
      else
        @tenants = Tenant.where("(status_id is null or status_id <> #{status.id}) and 
        id = #{params[:tenant_id]}").order("name")
        @total = @tenants.size
      end
    end
    @tenants = ActiveModelSerializers::SerializableResource.new(@tenants, each_serializer: TenantSerializer)
    render json: {tenants:@tenants, total:@total}
  end

  # POST /tenants
  def create
    @tenant = Tenant.create!(tenant_params)
    json_response(@tenant, :created)
  end

  # GET /tenants/:id
  def show
    json_response(@tenant)
  end

  # PUT /tenants/:id
  def update
    @tenant.update(tenant_params)
    head :no_content
  end

  # DELETE /tenants/:id
  def destroy
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      tenant = Tenant.find(params[:id])
      tenant.status_id = status.id
      if tenant.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  def destroy_tenant
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      tenant = Tenant.find(params[:id])
      tenant.status_id = status.id
      if tenant.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  private

  def tenant_params
    # whitelist params
    params.require(:tenant).permit(:name,:department_id,:city_id,:document_acceptance_text)
  end

  def set_tenant
    current_user = User.where("email = '#{params[:email]}'").first
    #if current_user.all_tenant == true
      @tenant = Tenant.find(params[:id])
    #end
  end
end
