class FirmantesController < ApplicationController
	
	def validate_email
		response = FirmanteService.validate_email(params[:email])
		json_response(response)
	end

	def validate_nit
		response = FirmanteService.validate_nit(params[:nit])
		json_response(response)
	end

	def signatory_search
		response = FirmanteService.search_signer_by_nit(params[:document])
		render json: {signatory: response}
	end
end