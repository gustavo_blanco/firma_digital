class UsersController < ApplicationController
	before_action :set_user, only: [:show, :update]
	before_action :authenticate_user!, except: [:get_user_by_email]
	# GET /users
	def index
		current_user = User.where("email = '#{params[:user_email]}'").first
		status = Status.find_by_name("ANULADO")
		if !current_user.blank? && current_user.all_tenant == true
			@users = User.where("status_id is null or status_id <> #{status.id}").
			paginate(:page =>params[:page],:per_page => 10).order(:name)
		elsif !current_user.blank?
			role = Role.find_by_name("ADMINISTRADOR")
			if current_user.role_id == role.id
				@users = User.where("(status_id is null or status_id <> #{status.id}) and
				tenant_id = #{current_user.tenant_id}").paginate(:page =>params[:page],:per_page => 10).order(:name)
			else
				@users = User.where("(status_id is null or status_id <> #{status.id}) and
				id = #{current_user.id}").paginate(:page =>params[:page],:per_page => 10).order(:name)
			end
		end
		@total = @users.total_entries
		@users = ActiveModelSerializers::SerializableResource.new(@users, each_serializer: UserSerializer)
		render json: {users:@users, total:@total}
	end

	# POST /users
	def create
		@user = User.new(user_params)
	    @user.confirmed_at = DateTime.now
	    @user.uid = @user.email
	    @user.provider = "email"
	    role = Role.find(@user.role_id)
	    @user.role_name = role.name
	    tenant = Tenant.find(@user.tenant_id)
	    @user.tenant_name = tenant.name
	    if role.name == "REPRESENTANTE_LEGAL"
    		@user.signer_token = SecureRandom.uuid
	    end
	    if @user.save
	      json_response(@user, :created)
	    else
	      json_response( @user.errors.full_messages )
	      # json_response(@user, :unprocessable_entity)
	    end
	end

	def search
		current_user = User.where("email = '#{params[:email]}'").first
		if current_user.all_tenant == true
			@users = User.search(params[:search_string],nil,params[:page])
		else
			@users = User.search(params[:search_string],current_user.tenant_id,params[:page])
		end
		#json_response(users)
		@total = @users.total_entries
		@users = ActiveModelSerializers::SerializableResource.new(@users, each_serializer: UserSerializer)
		render json: {users:@users, total:@total}
  	end

	# GET /users/:id
	def show
		json_response(@user)
	end

	# PUT /users/:id
	def update
		@user.update(user_params)
		@user.status_id = 1
		role = Role.find(@user.role_id)
	    @user.role_name = role.name
	    tenant = Tenant.find(@user.tenant_id)
	    @user.tenant_name = tenant.name
	    @user.save
		head :no_content
	end

	# DELETE /users/:id
	def destroy
		@response = "ERROR"
		status = Status.find_by_name("ANULADO")
		if !status.blank?
		  user = User.find(params[:id])
		  user.status_id = status.id
		  if user.save
		    @response = "OK"
		  end
		end
		json_response(@response)
	end

	def destroy_user
		@response = "ERROR"
		status = Status.find_by_name("ANULADO")
		if !status.blank?
		  user = User.find(params[:id])
		  user.status_id = status.id
		  if user.save
		    @response = "OK"
		  end
		end
		json_response(@response)
	end

	def get_user_documents
		user_services = UserService.new
		documents = user_services.get_user_documents(params[:id])
		json_response(documents, :created)
	end

	def set_user_documents
		user_services = UserService.new
		response = user_services.set_user_documents(params[:document])
		json_response(response)
	end

	def update_user_documents
		user_services = UserService.new
		response = user_services.update_user_documents(params[:document])
		json_response(response)
	end

	def destroy_user_documents
		user_services = UserService.new
		response = user_services.destroy_user_documents(params[:id])
		json_response(response)
	end

	def get_tenant_user
		tenant_name = ""
		tenant_id = nil
		if !params[:token].blank?
			tenant = Tenant.find(params[:tenant_id])
			if !tenant.blank?
				tenant_name = tenant.name
				tenant_id = tenant.id
			end
		end
		json_response({tenant_name: tenant_name,tenant_id: tenant_id})
	end

	def get_legal_representative
		role = Role.find_by_name("REPRESENTANTE_LEGAL")
		users = User.select("id,name,last_name,document,email,tenant_id").where("tenant_id = #{params[:tenant_id]} and
			role_id = #{role.id}")
		render json: {users:users}
	end

	def get_user_by_email
		current_user = User.where("email = '#{params[:email]}'").first
		json_response(current_user)
	end

	private

  	def user_params
    	# whitelist params
    	params.require(:user).permit(:name,:last_name,:document_type_id,:document,:phone,:address,
			:city_id,:department_id,:email,:password,:password_confirmation,
			:tenant_id,:user_type,:role_id)
  	end

	def set_user
		current_user = User.where("email = '#{params[:email]}'").first
		if current_user.all_tenant == true
			@user = User.where("id = #{params[:id]}").first
		else
			if current_user.role_name == "ADMINISTRADOR"
				@user = User.where("id = #{params[:id]}").first
			else
				@user = User.where("id = #{params[:id]} and email = '#{params[:email]}'").first
			end
		end
	end
end
