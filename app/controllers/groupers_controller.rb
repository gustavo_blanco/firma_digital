class GroupersController < ApplicationController
  before_action :set_grouper, only: [:show, :update]
  before_action :authenticate_user!
  # GET /groupers
  def index
    status = Status.find_by_name("ANULADO")
    if !params[:page].blank?
      @groupers = Grouper.where("(status_id is null or status_id <> #{status.id}) and 
        tenant_id = #{params[:tenant_id]}").
      paginate(:page =>params[:page],:per_page => 10).order(:name)
      @total = @groupers.total_entries 
      render json: {groupers:@groupers, total:@total}
    else
      @groupers = Grouper.where("(status_id is null or status_id <> #{status.id}) and 
        tenant_id = #{params[:tenant_id]}").order('name asc')
      json_response(@groupers)
    end
  end

  # POST /groupers
  def create
    @grouper = Grouper.create!(grouper_params)
    json_response(@grouper, :created)
  end

  # GET /groupers/:id
  def show
    json_response(@grouper)
  end

  # PUT /groupers/:id
  def update
    @grouper.update(grouper_params)
    head :no_content
  end

  # DELETE /groupers/:id
  def destroy
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      grouper = Grouper.find(params[:id])
      grouper.status_id = status.id
      if grouper.save
        @response = "OK"
      end
    end 
  end

  def destroy_grouper
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      grouper = Grouper.find(params[:id])
      grouper.status_id = status.id
      if grouper.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  private

  def grouper_params
    # whitelist params
    params.require(:grouper).permit(:name,:tenant_id)
  end

  def set_grouper
    current_user = User.where("email = '#{params[:email]}'").first
    if current_user.role_name == "ADMINISTRADOR"
      @grouper = Grouper.where("id = #{params[:id]} and tenant_id = #{current_user.tenant_id}").first
    end
  end
end
