class DocumentTypesController < ApplicationController
  before_action :set_document_type, only: [:show, :update, :destroy]

  # GET /cities
  def index
    status = Status.find_by_name("ANULADO")
    @document_types = DocumentType.where("status_id is null or status_id <> #{status.id}").order('name asc')
    json_response(@document_types)
  end

  # POST /cities
  def create
    @document_type = DocumentType.new(document_type_params)
    if @document_type.save
        render json: @document_type
    else
        render json: { errors: @document_type.errors.full_messages }, status: 422
    end
  end

  # GET /cities/:id
  def show
    json_response(@document_type)
  end

  # PUT /cities/:id
  def update
    @document_type.update(document_type_params)
    head :no_content
  end

  # DELETE /cities/:id
  def destroy
    @document_type.destroy
    head :no_content
  end

  def destroy_document_type
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      document_type = DocumentType.find(params[:id])
      document_type.status_id = status.id
      if document_type.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  private

  def document_type_params
    # whitelist params
    params.require(:document_type).permit(:name)
  end

  def set_document_type
    @document_type = DocumentType.find(params[:id])
  end
end
