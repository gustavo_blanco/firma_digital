class DocumentsController < ApplicationController
	before_action :set_document, only: [:show, :update, :destroy]
	before_action :authenticate_user!, except: [:show,:validate_token,:set_firm_document,
		:validate_reject_token,:reject_document]
	# GET /documents
	def index
		#DocumentService.send_notifaction_expired_documents
		status = Status.find_by_name("ANULADO")
		status_r = Status.find_by_name("RECHAZADO")
		current_user = User.where("email = '#{params[:user_email]}'").first
		if !current_user.blank? && current_user.all_tenant == true
			@documents = Document.where("status_id is null or status_id <> #{status.id}").
			paginate(:page =>params[:page],:per_page => 10).order(:name)
		elsif !current_user.blank?
			@documents = Document.documents_per_rol(current_user,params[:tenant_id],params[:page],status.id)
		end
		@total = @documents.total_entries
		@documents = ActiveModelSerializers::SerializableResource.new(@documents, each_serializer: DocumentsSerializer)
		render json: {documents:@documents, total:@total}
	end

	# POST /documents
	def create
		@document = DocumentService.set_user_documents(params[:document],params[:ip_address])
	    if @document
	      	json_response(@document, :created)
	    else
	      json_response(@document, :unprocessable_entity)
	    end
	end

	def validate_token
		validation = params[:validation]
		where_str = "token_login = '#{validation["token"].strip}' and document_id = #{validation["document_id"]} and email = '#{validation["email"]}'"
		firmante = Firmante.where(where_str).first
		#firmante = Firmante.where("token_login = ? and document_id = ? and email = ?",validation["token"],validation["document_id"],validation["email"]).first
		unless firmante.blank?
			event_name = "Ingreso exitoso al documento por parte de #{firmante.nombre}"
			msg = "OK"
		else
			firmante=Firmante.where("email = '#{validation["email"]}'").first
			if !firmante.blank?
				event_name = "Ingreso invalido por parte de #{firmante.nombre}"
			else
				event_name = "Ingreso invalido"
			end 
			msg = "ERROR"
		end
		EventLogService.set_event_log(event_name,validation["document_id"],params[:ip_address])
		json_response({msg:msg})
	end

	def validate_reject_token
		where_str = "signer_token = '#{params["reject_token"].strip}' and document_id = #{params["id"]} and email = '#{params["email"]}'"
		firmante = Firmante.where(where_str).first
		if !firmante.blank?
			event_name = "Ingreso exitoso al rechazo de documento por parte de #{firmante.nombre}"
			msg = "OK"
		else
			firmante=Firmante.where("email = '#{params["email"]}'").first
			if !firmante.blank?
				event_name = "Ingreso de token invalido para rechazar documento por parte de #{firmante.nombre}"
			else
				event_name = "Ingreso de token invalido para rechazar documento"
			end 
			msg = "ERROR"
		end
		EventLogService.set_event_log(event_name,params["id"],params[:ip_address])
		json_response({msg:msg})
	end

	def search
		status = Status.find_by_name("ANULADO")
		current_user = User.where("email = '#{params[:user_email]}'").first
		if current_user.all_tenant == true
			@documents = Document.search(params[:search_string],nil,params[:page])
		else
			if params[:search_string].blank?
				@documents = Document.documents_per_rol(current_user,params[:tenant_id],params[:page],status.id)
			else
				@documents = Document.search(params[:search_string],current_user,params[:page])
			end
		end
		@total = @documents.total_entries
		@documents = ActiveModelSerializers::SerializableResource.new(@documents, each_serializer: DocumentsSerializer)
		render json: {documents:@documents, total:@total}
  	end

	# GET /documents/:id
	def show
		#current_user = User.where("email = '#{params[:user_email]}'").first
		json_response(@document)
	end

	# PUT /documents/:id
	def update
		@document.update(document_params)
		head :no_content
	end

	# DELETE /documents/:id
	def destroy
		@response = "ERROR"
		status = Status.find_by_name("ANULADO")
		if !status.blank?
		  document = Document.find(params[:id])
		  document.status_id = status.id
		  if document.save
		    @response = "OK"
		  end
		end
		json_response(@response)
	end

	def destroy_document
		@response = "ERROR"
		status = Status.find_by_name("ANULADO")
		if !status.blank?
		  document = Document.find(params[:id])
		  document.status_id = status.id
		  if document.save
		    @response = "OK"
		  end
		end 
		json_response(@response)
	end

	def set_firm_document
		response = "ERROR"
		if DocumentService.validate_token(params[:id],params[:token].strip,params[:email])
			firmante = Firmante.where("document_id = #{params[:id]} and signer_token = '#{params[:token].strip}'").first
			document = Document.find(params[:id])
			#document = DocumentService.accept_document(document.attachment.s3_object.key,document.attachment_file_name,document.id,firmante)
			document = DocumentService.accept_document(document.attachment,document.attachment_file_name,document.id,firmante,params[:ip_address])
			event_name = FirmanteService.get_firmante_name_by_token(document,params[:token].strip)
			FirmanteService.set_signed_document(params[:id],params[:token].strip)
			EventLogService.set_event_log(event_name,params[:id],params[:ip_address])
			if document.flujo == "Secuencial"
				EventLogService.set_event_next_firmante(document,params[:token].strip,params[:ip_address])	
			end
			document = DocumentService.validate_all_signed(params[:id])
		else
			firmante = Firmante.where("email = '#{params[:email]}' and document_id = #{params[:id]}").first
			if !firmante.blank?
				event_name = "Token invalido ingresado por parte de #{firmante.nombre}"
			else
				event_name = "Ingreso invalido"	
			end
			EventLogService.set_event_log(event_name,params[:id],params[:ip_address])
		end
		json_response(document)
	end

	def reject_document 
		@response = "ERROR"
		if DocumentService.validate_token(params[:id],params[:token].strip,params[:email])
			status = Status.find_by_name("RECHAZADO")
			if !status.blank?
			  document = Document.find(params[:id])
			  document.status_id = status.id
			  document.reason_rejection = params[:reason]
			  if document.save
			  	firmante = Firmante.where("email = '#{params[:email]}' and document_id = #{params[:id]}").first
			    if !firmante.blank?
			    	if document.flujo == "Secuencial"
						firmantes = Firmante.where("document_id = #{document.id} and rol <> 'Firmante'").order("orden_firmante")
					else
						firmantes = Firmante.where("document_id = #{document.id} and rol <> 'Firmante'").order("id")   
					end
					if !firmantes.blank?
				    	event_name = "Documento Rechazado por #{firmante.nombre} \nRazón: #{document.reason_rejection}"
				    	EventLogService.set_event_log(event_name,params[:id],params[:ip_address])
						firmantes.each do |f|
					    	DocumentMailer.send_reject_document_confirmation(document, firmante,f).deliver!
						end
					end
			    end
			    @response = "OK"
			  end
			end
		end 
		json_response({msg: @response})
	end

	def get_document_documents
		document_services = documentservice.new
		documents = document_services.get_document_documents(params[:id])
		json_response(documents, :created)
	end

	def set_document_documents
		document_services = documentservice.new
		response = document_services.set_document_documents(params[:document])
		json_response(response)
	end

	def update_document_documents
		document_services = documentservice.new
		response = document_services.update_document_documents(params[:document])
		json_response(response)
	end

	def destroy_document_documents
		document_services = documentservice.new
		response = document_services.destroy_document_documents(params[:id])
		json_response(response)
	end

	def send_notification
		#raise params[:notification][:code].inspect
		if !params[:notification].blank?
			document = Document.find(params[:notification][:document_id])
			if params[:notification][:code] == "reenvio_notificacion_firmante"
=begin
				firmante=Firmante.find(params[:notification][:firmante_id])
				event_name = "Envío de token para firmar documento a #{firmante.nombre}"
				DocumentMailer.send_token(document, firmante).deliver!
				EventLogService.set_event_log(event_name,document.id)
=end			
				firmantes = Firmante.where("document_id = #{document.id} and rol = 'Firmante' and
					 signed is null").order("orden_firmante")
				if document.flujo == "Secuencial"
					if !firmantes.blank?
						event_name = "Envío de token para firmar documento a #{firmantes.first.nombre}"
						DocumentMailer.send_token(document, firmantes.first).deliver!
						EventLogService.set_event_log(event_name,document.id,params[:ip_address])
					end
				else	
					if !firmantes.blank?
						firmantes.each do |firmante|
							event_name = "Envío de token para firmar documento a #{firmante.nombre}"
							DocumentMailer.send_token(document, firmante).deliver!
							EventLogService.set_event_log(event_name,document.id,params[:ip_address])
						end
					end
				end
				status = "OK"
			elsif params[:notification][:code] == "rechazo"
				status = Status.find_by_name("RECHAZADO")
				document.status_id = status.id
				document.save
				usuario = User.where("email = '#{params[:email]}'").first
				if document.flujo == "Secuencial"
					firmantes = Firmante.where("document_id = #{document.id}").order("orden_firmante")
				else
					firmantes = Firmante.where("document_id = #{document.id}").order("id")   
				end
				if !firmantes.blank?
			    	event_name = "Documento Rechazado por #{usuario.name}"
			    	EventLogService.set_event_log(event_name,document.id,params[:ip_address])
					firmantes.each do |f|
				    	DocumentMailer.send_reject_document_confirmation_admin(document, usuario,f).deliver!
					end
				end
				status = "OK"
			elsif params[:notification][:code] == "aceptacion"
				if document.flujo == "Secuencial"
					firmantes = Firmante.where("document_id = #{document.id}").order("orden_firmante")
				else
					firmantes = Firmante.where("document_id = #{document.id}").order("id")   
				end
				status = Status.find_by_name("FINALIZADO")
				document.status_id = status.id
				document.save
				firmantes.each do |firmante|
					DocumentMailer.send_confirmation(document, firmante).deliver!
				end	
				status = "OK"	
			end
		end
		document = ActiveModelSerializers::SerializableResource.new(document, serializer: DocumentSerializer)
		json_response({msg:status,document:document})
	end

	private

  	def document_params
    	# whitelist params
    	params.require(:document).permit(:name,:attachment,:grouper_id,:status_id,:objeto_costo,
			:radicado,:token_email,:token_mobile,:plataforma_origen,:fecha_limite_aprobacion,
			:token,:usuario_company, :flujo,:tenant_id)
  	end

	def set_document
		status = Status.find_by_name("ANULADO")
		if params[:perfil] == "user"
			client = request.headers['client']
			token = request.headers['access-token']
			if current_user.token_is_current?(token,client)
				current_user = User.where("email = '#{params[:user_email]}'").first
				if current_user.all_tenant == true
					@document = Document.where("id = #{params[:id]} and status_id <> #{status.id}").first
					#@document = Document.find(params[:id])
				else
					@document = Document.where("id = #{params[:id]} and tenant_id = #{params[:tenant_id]} and 
						status_id <> #{status.id}").first
				end
			end
		else
			firmante = Firmante.where("email = '#{params[:user_email]}' and token_login = '#{params[:token].strip}'").first
			if !firmante.blank?
				#@document = Document.find(params[:id])
				@document = Document.where("id = #{params[:id]} and status_id <> #{status.id}").first
			end
		end
	end
end
