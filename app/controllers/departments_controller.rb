class DepartmentsController < ApplicationController
  before_action :set_department, only: [:show, :update]
  before_action :authenticate_user!

  # GET /cities
  def index
    status = Status.find_by_name("ANULADO")
    if !params[:page].blank?
      @departments = Department.where("(status_id is null or status_id <> #{status.id}) and 
        tenant_id = #{params[:tenant_id]}").
      paginate(:page =>params[:page],:per_page => 10).order(:name)
      @total = @departments.total_entries 
      render json: {departments:@departments, total:@total} 
    else
      @departments = Department.where("(status_id is null or status_id <> #{status.id}) and 
        tenant_id = #{params[:tenant_id]}").order('name asc')
      json_response(@departments)
    end
  end

  # POST /cities
  def create
    @department = Department.new(department_params)
    if @department.save
        render json: @department
    else
        render json: { errors: @department.errors.full_messages }, status: 422
    end
  end

  # GET /cities/:id
  def show
    json_response(@department)
  end

  # PUT /cities/:id
  def update
    @department.update(department_params)
    head :no_content
  end

  # DELETE /cities/:id
  def destroy
   @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      department = Department.find(params[:id])
      department.status_id = status.id
      if department.save
        @response = "OK"
      end
    end 
  end

  def destroy_department
    @response = "ERROR"
    status = Status.find_by_name("ANULADO")
    if !status.blank?
      department = Department.find(params[:id])
      department.status_id = status.id
      if department.save
        @response = "OK"
      end
    end 
    json_response(@response)
  end

  private

  def department_params
    # whitelist params
    params.require(:department).permit(:name,:tenant_id)
  end

  def set_department
    current_user = User.where("email = '#{params[:email]}'").first
    if current_user.role_name == "ADMINISTRADOR"
      @department = Department.where("id = #{params[:id]} and tenant_id = #{current_user.tenant_id}").first
    end
  end
end
