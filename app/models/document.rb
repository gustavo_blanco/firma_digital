class Document < ApplicationRecord

	has_many :firmantes, -> { order 'posicion_firma' }
	has_many :event_logs
=begin	
	has_attached_file :attachment,
		:storage => :s3,
		:s3_region => 'us-east-1',
		:s3_credentials => {
			:access_key_id => "AKIAIRI2LAO757MQ3LIA",
			:secret_access_key => "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",
			:bucket=> "conconcreto-firmadigital",
	    },
		:path => "/documents/:document_id/:basename.:extension"

	#vuelta de google
	has_attached_file :attachment,
	  storage: :gcs,
	  gcs_bucket: "mabella-fdigital",
	  gcs_credentials: {
	    project: "gcp-conconcreto-mabella",
	    keyfile: "/home/yeryie/Proyectos/firma_digital/gcp-conconcreto-mabella-72e48074ecfc.json",
	  },
	  :path => "/documents/:document_id/:basename.:extension"
=end
	
	has_attached_file :attachment,
	 	storage: :fog,
	  	:fog_credentials => {provider: 'Google', 
			google_storage_access_key_id: 'GOOGEQUAQ6KMVPFG6QL4ZWY6', 
			google_storage_secret_access_key: '8s17TS6llERWzIvFBfj3al3TjIBhIM4ApV4bu4n3'},
    	:fog_directory => "mabella-fdigital"
	
	do_not_validate_attachment_file_type :attachment

	def self.search(termino,current_user,page)
    	status = Status.find_by_name("ANULADO")
		join_str = "JOIN firmantes f ON f.document_id = documents.id"
		if !termino.blank?
	        if !current_user.blank?
				if current_user.role_name == "ADMINISTRADOR"
					where_str = "((concat(upper(name)) like '%#{termino.upcase}%' or concat(upper(objeto_costo)) like '%#{termino.upcase}%') and tenant_id = #{current_user.tenant_id}) and
	          (documents.status_id is null or documents.status_id <> #{status.id})"
				else
					where_str = "((concat(upper(name)) like '%#{termino.upcase}%' or concat(upper(objeto_costo)) like '%#{termino.upcase}%') and tenant_id = #{current_user.tenant_id}) and f.user_id = #{current_user.id} and
	          (documents.status_id is null or documents.status_id <> #{status.id})"
				end
	          
	        else
	          where_str = "(concat(upper(name)) like '%#{termino.upcase}%' or concat(upper(objeto_costo)) like '%#{termino.upcase}%' ) and 
	          (documents.status_id is null or documents.status_id <> #{status.id})"
	        end
	    else
	        if !current_user.blank?
	          where_str = "((documents.status_id is null or documents.status_id <> #{status.id}) and tenant_id = #{current_user.tenant_id} and f.user_id = #{current_user.id})"
	        else
	          where_str = "(documents.status_id is null or documents.status_id <> #{status.id})"
	        end
    	end
  		Document.joins(join_str).where(where_str).paginate(:page =>page,:per_page => 10).order('name asc').distinct
	end

	def self.documents_per_rol(current_user,tenant_id,page,status_id)
		if !current_user.blank? && current_user.role_name == "REPRESENTANTE_LEGAL"
			status_pendiente = Status.find_by_name("PENDIENTE POR FIRMAS")
			join_str = "JOIN firmantes f ON f.document_id = documents.id"
			documents = Document.joins(join_str).where("(documents.status_id is null or 
				documents.status_id = #{status_pendiente.id}) and documents.tenant_id = #{tenant_id} and 
				f.user_id = #{current_user.id}").paginate(:page =>page,:per_page => 10).order(:name)
		else
			documents = Document.where("(status_id is null or status_id <> #{status_id}) and 
				tenant_id = #{tenant_id}").paginate(:page =>page,:per_page => 10).order(:name)
		end
		return documents
	end


	def download_url_antiguo
		url = ""
  		if !self.attachment.blank?	
  			return self.attachment.public_url
			key = self.attachment.s3_object.key		
			expires_in = 3600
			access_key = "AKIAIRI2LAO757MQ3LIA"
			secret_key = "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"
			creds = Aws::Credentials.new(access_key, secret_key)
	  		client = Aws::S3::Client.new(region: 'us-east-1', credentials: creds)
			@signer = Aws::S3::Presigner.new(client: client)
			url = @signer.presigned_url(:get_object, bucket: 'conconcreto-firmadigital', key: key, expires_in: expires_in)
		#raise url.to_s.inspect
			return url.to_s
		end
	end

	def download_url
		url = ""
  		if !self.attachment.blank?
  			if self.attachment.exists?
  				url = self.attachment.public_url	
  			end	
			return url
		end
	end

	private 

	    #interpolate
	    Paperclip.interpolates :document_id do |attachment, style|
	      document_id = attachment.instance.id
	    end
end
