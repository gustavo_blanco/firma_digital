class DocumentType < ApplicationRecord
	validates :name, uniqueness: true
end
