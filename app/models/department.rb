class Department < ApplicationRecord

	validates :name, uniqueness: { scope: :tenant_id}
end
