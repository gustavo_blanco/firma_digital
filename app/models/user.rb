class User < ApplicationRecord
    # Include default devise modules.
    devise :database_authenticatable, :registerable,
	:recoverable, :rememberable, :trackable, :validatable,:omniauthable
    include DeviseTokenAuth::Concerns::User

    # validates :email, format: { with: /\A(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{6,16}*\z/i, message: "Formato de Email Incorrecto"}
    def self.search(termino,tenant_id,page)
  		if !termino.blank?
        if !tenant_id.blank?
          where_str = "(concat(upper(name),' ',upper(last_name)) like '%#{termino.upcase}%' or
            document like '%#{termino.upcase}%') and tenant_id = #{tenant_id}"
        else
          where_str = "(concat(upper(name),' ',upper(last_name)) like '%#{termino.upcase}%' or
            document like '%#{termino.upcase}%')"
        end
    	else
    		status = Status.find_by_name("ANULADO")
        if !tenant_id.blank?
          where_str = "(status_id is null or status_id <> #{status.id}) and tenant_id = #{tenant_id}"
        else
          where_str = "(status_id is null or status_id <> #{status.id})"
        end
    	end
  		User.where(where_str).paginate(:page =>page,:per_page => 10).order('name asc')
  	end
end
