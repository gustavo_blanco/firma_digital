class TenantSerializer < ActiveModel::Serializer
	attributes :id,:name ,:department_id,:city_id,:department_name,:city_name,:document_acceptance_text

	def department_name
		name = ""
		if !object.department_id.blank?
	    	name =Department.find(object.department_id).name
		end
		return name
	end

	def city_name
		name = ""
		if !object.city_id.blank?
			name = City.find(object.city_id).name
		end
		return name
	end
end
