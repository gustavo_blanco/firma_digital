class FirmantesSerializer < ActiveModel::Serializer
  	attributes :nombre,:nit,:email,:celular,:rol,:posicion_firma,:orden_firmante,:status_id,
  	:created_at,:updated_at,:document_id,:signer_token
end
