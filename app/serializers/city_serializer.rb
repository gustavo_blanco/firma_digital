class CitySerializer < ActiveModel::Serializer
	attributes :id,:name ,:department_id,:department_name

	def department_name
	    Department.find(object.department_id).name
	end
end
