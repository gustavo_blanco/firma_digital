class UserSerializer < ActiveModel::Serializer
  attributes :id,:name,:last_name,:document_type_id,:document,:phone,:address,
	:city_id,:department_id,:email,:password,:password_confirmation,
	:tenant_id,:user_type,:department_name,:city_name,:document_type_name,:tenant_name,
	:role_id,:role_name,:signer_token


	def department_name
		name = ""
		if !object.department_id.blank?
	    	name = Department.find(object.department_id).name
		end
	    return name
	end

	def city_name
		name = ""
		if !object.city_id.blank?
	    	name = City.find(object.city_id).name
		end
		return name
	end

	def document_type_name
		name = ""
		if !object.document_type_id.blank?
	    	DocumentType.find(object.document_type_id).name
		end
		return name
	end

	def tenant_name
		tenant = ""
		if !object.tenant_id.blank?	
			tenant = Tenant.find(object.tenant_id).name
		end
		return tenant
	end

	def role_name
		name = ""
		if !object.role_id.blank?
			name = Role.find(object.role_id).name
		end
		return name
	end
end
