class DocumentSerializer < ActiveModel::Serializer
  	attributes :id,:name ,:grouper_id,:grouper_name,:document_type_id,:grouper_id,:status_id,
	:objeto_costo,:radicado,:token_email,:token_mobile,:plataforma_origen,
	:fecha_limite_aprobacion,:usuario_company,:attachment_file_name,:attachment_content_type,
	:attachment_file_size,:attachment_updated_at,:download,:flujo,:reason_rejection,:status_name,
	:tenant_id,:tenant_document_acceptance_text

	has_many :firmantes, :order => :orden_firmante
	has_many :event_logs

  	def grouper_name
  		grouper_name = ""
  		if !object.grouper_id.blank?
	    	grouper_name =Grouper.find(object.grouper_id).name
  		end
  		return grouper_name
	end

	def user_id
		DocumentUser.where("document_id = #{object.id}").first.user_id
	end

	def download	
		url = object.download_url
		return url.to_s
	end

	def status_name
  		status_name = DocumentService.get_doc_status_name(object)
  		return status_name
	end

	def tenant_document_acceptance_text
		name = ""
  		if !object.tenant_id.blank?
	    	name =Tenant.find(object.tenant_id).document_acceptance_text
  		end
  		return name
	end
end
