class EventLogSerializer < ActiveModel::Serializer
  	attributes :id,:name,:document_id,:description,:created_at,:ip_address
end
