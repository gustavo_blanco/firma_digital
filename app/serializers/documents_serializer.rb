class DocumentsSerializer < ActiveModel::Serializer
  	attributes :id,:name ,:grouper_id,:grouper_name,:grouper_id,:status_id,
	:radicado,:plataforma_origen,
	:fecha_limite_aprobacion,:usuario_company,:flujo,:status_name,
	:tenant_id, :objeto_costo

  	def grouper_name
  		grouper_name = ""
  		if !object.grouper_id.blank?
	    	grouper_name =Grouper.find(object.grouper_id).name
  		end
  		return grouper_name
	end

	def status_name
  		status_name = DocumentService.get_doc_status_name(object)
  		return status_name
	end
end
