# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#User.create(email:'admin@admin.com',nickname:'admin', name: 'Admin', password: 'admin123')
#status = Status.create([{ name: 'ACTIVO' }, { name: 'ANULADO' }])
#status = Status.create({name: 'RECHAZADO'})
#admin = User.where("email = 'admin@admin.com'").first
#admin.all_tenant = true
#admin.save
#status = Status.create({name: 'FINALIZADO'})
#status = Status.create({name: 'PENDIENTE POR FIRMAS'})

#role = Role.create({name: 'ADMINISTRADOR'})
#role = Role.create({name: 'REPRESENTANTE_LEGAL'})

users = User.all
if !users.blank?
	users.each do |user|
		if !user.role_id.blank?
			role = Role.find(user.role_id)
	    	user.role_name = role.name
		end
		if !user.tenant_id.blank?
			tenant = Tenant.find(user.tenant_id)
	    	user.tenant_name = tenant.name
		end
		user.save
	end
end
