class CreateGroupers < ActiveRecord::Migration[5.2]
  def change
    create_table :groupers do |t|
      t.string :name
      t.integer :status_id

      t.timestamps
    end
  end
end
