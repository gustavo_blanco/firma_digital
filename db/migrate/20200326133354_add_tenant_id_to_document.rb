class AddTenantIdToDocument < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :tenant_id, :integer
  end
end
