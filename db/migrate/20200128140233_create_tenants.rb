class CreateTenants < ActiveRecord::Migration[5.2]
  def change
    create_table :tenants do |t|
    	t.string 	:name
    	t.integer	:department_id
    	t.integer	:city_id
    	t.integer	:status_id
  		t.timestamps
    end
  end
end
