class AddDocumentAcceptanceTextToTenant < ActiveRecord::Migration[5.2]
  def change
    add_column :tenants, :document_acceptance_text, :string
  end
end
