class AddStatusIdToDocument < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :status_id, :integer
  end
end
