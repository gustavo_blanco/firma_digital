class AddUserIdToFirmante < ActiveRecord::Migration[5.2]
  def change
    add_column :firmantes, :user_id, :integer
  end
end
