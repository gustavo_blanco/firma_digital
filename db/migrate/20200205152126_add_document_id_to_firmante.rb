class AddDocumentIdToFirmante < ActiveRecord::Migration[5.2]
  def change
    add_column :firmantes, :document_id, :integer
  end
end
