class AddReasonRejectionToDocument < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :reason_rejection, :string
  end
end
