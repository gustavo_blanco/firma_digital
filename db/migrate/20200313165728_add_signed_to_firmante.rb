class AddSignedToFirmante < ActiveRecord::Migration[5.2]
  def change
    add_column :firmantes, :signed, :boolean
  end
end
