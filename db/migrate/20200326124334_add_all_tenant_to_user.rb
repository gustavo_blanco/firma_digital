class AddAllTenantToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :all_tenant, :boolean
  end
end
