class AddIpAddressToEventLog < ActiveRecord::Migration[5.2]
  def change
    add_column :event_logs, :ip_address, :string
  end
end
