class AddTenantIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :tenant_id, :integer
    add_column :users, :user_type, :string
  end
end
