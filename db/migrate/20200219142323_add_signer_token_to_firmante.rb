class AddSignerTokenToFirmante < ActiveRecord::Migration[5.2]
  def change
  	add_column :firmantes, :signer_token, :string
  end
end
