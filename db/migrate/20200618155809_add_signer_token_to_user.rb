class AddSignerTokenToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :signer_token, :string
  end
end
