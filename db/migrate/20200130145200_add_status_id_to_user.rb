class AddStatusIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :status_id, :integer
  end
end
