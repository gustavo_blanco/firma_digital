class CreateFirmanteDocumentos < ActiveRecord::Migration[5.2]
  def change
    create_table :firmante_documentos do |t|
      t.integer :firmante_id
      t.integer :document_id

      t.timestamps
    end
  end
end
