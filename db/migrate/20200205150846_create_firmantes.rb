class CreateFirmantes < ActiveRecord::Migration[5.2]
  def change
    create_table :firmantes do |t|
    	t.string	:nombre
    	t.string	:nit
    	t.string	:email
    	t.string	:celular
    	t.string	:rol
    	t.integer	:posicion_firma
    	t.integer	:orden_firmante
    	t.integer	:status_id
		t.timestamps
    end
  end
end
