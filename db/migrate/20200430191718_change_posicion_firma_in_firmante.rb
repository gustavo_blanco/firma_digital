class ChangePosicionFirmaInFirmante < ActiveRecord::Migration[5.2]
  def change
  	change_column(:firmantes, :posicion_firma, :string)
  end
end
