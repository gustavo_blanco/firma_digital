class AddRejectTokenToFirmantes < ActiveRecord::Migration[5.2]
  def change
    add_column :firmantes, :reject_token, :string
  end
end
