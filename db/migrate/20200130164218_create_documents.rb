class CreateDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :documents do |t|
    	t.string	:name
    	t.integer	:document_type_id
    	t.integer	:grouper_id
		t.timestamps
    end
  end
end
