class AddTenantToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :tenant_id, :integer
    add_column :departments, :tenant_id, :integer
    add_column :groupers, :tenant_id, :integer
  end
end
