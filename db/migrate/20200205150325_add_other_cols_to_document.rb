class AddOtherColsToDocument < ActiveRecord::Migration[5.2]
	def change
		add_column :documents, :objeto_costo, :string
		add_column :documents, :radicado, :string
		add_column :documents, :token_email, :string
		add_column :documents, :token_mobile, :string
		add_column :documents, :plataforma_origen, :string
		add_column :documents, :fecha_limite_aprobacion, :date
		add_column :documents, :token, :string
		add_column :documents, :usuario_company, :string
	end
end
