class AddTenantNameToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :tenant_name, :string
  end
end
