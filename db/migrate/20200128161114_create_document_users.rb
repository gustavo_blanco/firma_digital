class CreateDocumentUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :document_users do |t|
    	t.integer	:document_id
    	t.integer	:user_id
      	t.timestamps
    end
  end
end
