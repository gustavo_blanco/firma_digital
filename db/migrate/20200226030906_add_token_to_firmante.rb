class AddTokenToFirmante < ActiveRecord::Migration[5.2]
  def change
    add_column :firmantes, :token_login, :string
  end
end
