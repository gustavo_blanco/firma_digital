class CreateEventLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :event_logs do |t|
		t.string 	:name
		t.integer	:document_id
		t.string	:description
		t.timestamps
    end
  end
end
